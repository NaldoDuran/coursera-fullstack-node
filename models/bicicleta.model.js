var Bicicleta = function(id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.allBicis = [];

Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId){
  var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId)
  if(aBici){
    return aBici;
  }else{
    return new Error(`No existe una bicicleta con el id ${aBiciId}`);
  }
}

Bicicleta.removeById = function(aBiciId){
  for(var i = 0; i < Bicicleta.allBicis.length; i++){
    if(Bicicleta.allBicis[i].id == aBiciId){
      Bicicleta.allBicis.splice(i, 1);
      break;
    }
  }
}

var bici1 = new Bicicleta(1, 'rojo', 'urbana', [11.011618,-74.823883]);
var bici2 = new Bicicleta(2, 'blue', 'urbana', [11.011518,-74.823883]);

Bicicleta.allBicis.push(bici1);
Bicicleta.allBicis.push(bici2);

module.exports = Bicicleta;
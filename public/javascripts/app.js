window.document.addEventListener('DOMContentLoaded', function () {
    var mymap = L.map('mapid').setView([11.001118,-74.823883], 13);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoibmFsZG9yY2siLCJhIjoiY2tjaHNsZm9hMGpsbTJ6cDc3YTU3OXd5OCJ9.TwbFktkPWTl4XpnK2icDJg'
    }).addTo(mymap);
    L.marker([11.011118,-74.823883]).addTo(mymap);
    L.marker([11.012118,-74.843883]).addTo(mymap);
    L.marker([11.001118,-74.823883]).addTo(mymap);
});